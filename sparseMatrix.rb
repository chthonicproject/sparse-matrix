############################################################
#                     sparseMatrix.rb                      #
#                                                          #
#  Author: Ritwik Banerjee <rbanerjee@cs.stonybrook.edu>   #
#                                                          #
# This file provides the Matrix class for sparse matrix    #
# representation and operations. Only the basic operations #
# have been provided so far.                               #
############################################################

require 'sparseMatrixErrors.rb'
require 'matrix'
require 'bigdecimal'

class Float
  def restrict(k)
    ((self * (10**k)).to_f/(10**k))
  end
end

module SparseMatrix
  class SpMatrix
    attr_accessor :row_dimension, :column_dimension, :matrix
    private_class_method :new
    $precision = 6

    def initialize(column_hash, column_expansion = true, col_dim = nil, row_dim = nil)
      t_keys = column_hash.keys
      t_value_keys = (column_hash.values.collect { |elem| elem.keys }).flatten
      begin
        keyvalidity = validkeys?(t_keys)
        valuevalidity = validkeys?(t_value_keys)
        raise InvalidKeyError.new(keyvalidity) unless keyvalidity
        raise InvalidKeyError.new(valuevalidity) unless valuevalidity
      rescue InvalidKeyError => err
        puts err.inspect
      end
      @matrix = column_hash
      if col_dim.nil?
        @column_dimension = t_keys.max
      else
        @column_dimension = col_dim
      end
      if row_dim.nil?
        @row_dimension = t_value_keys.max
      else
        @row_dimension = row_dim
      end
    end

    def SpMatrix.hash(column_hash, column_expansion = true, col_dim = nil, row_dim = nil)
      new(column_hash, column_expansion, col_dim, row_dim)
    end

    def SpMatrix.identity(size)
      id = Hash.new { |h,k| h[k] = Hash.new(0) }
      1.upto(size) { |ii| id[ii][ii] = 1 }
      new(id, true)
    end

    def SpMatrix.diagonal(*values)
      diag = Hash.new { |h,k| h[k] = Hash.new(0) }
      1.upto(values.size) { |ii| diag[ii][ii] = values[ii-1] }
      new(diag, true)
    end

    def row_dimension; @row_dimension; end
    def column_dimension; @column_dimension; end

    def validkeys? arr
      arr.each do |elem|
        begin
          (elem.integer?) ? (next) : (return false)
        rescue NoMethodError => err
          return elem
        end
      end
      return true
    end

    def addcells(x, y)
      if x.nil? and y.nil?
        warn "Redundant computation: returning zero."
        return 0
      end
      n1 = (x.nil?) ? 0 : x
      n2 = (y.nil?) ? 0 : y
      return n1 + n2
    end

    def +(m)
      unless ((row_dimension == m.row_dimension) and
              (column_dimension == m.column_dimension))
        raise DimensionMismatchError.new(row_dimension, column_dimension,
                                         m.row_dimension, m.column_dimension)
      end
      sumkeys = @matrix.keys | m.matrix.keys
      sumhash = Hash.new { |h,k| h[k] = Hash.new(0) }
      sumkeys.each do |col_ind|
        @matrix[col_ind].each_pair do |row_ind, value|
          sumhash[col_ind][row_ind] = addcells(value, m.matrix[col_ind][row_ind])
        end
      end
      SpMatrix.hash(sumhash, true)
    end

    def *(m)
      producthash = Hash.new { |h,k| h[k] = Hash.new(0) }
      case(m)
      when Numeric
        @matrix.each_pair do |col_ind, val|
          val.each_key do |row_ind|
            producthash[col_ind][row_ind] = ((@matrix[col_ind][row_ind] * m).to_f).restrict($precision)
          end
        end
        return SpMatrix.hash(producthash, true)
      when SparseMatrix::SpMatrix
        unless column_dimension == m.row_dimension
          raise DimensionMismatchError.new(row_dimension, column_dimension,
                                           m.row_dimension, m.column_dimension)
        end
        m.matrix.each_key do |jj|
          m.matrix[jj].each_key do |kk|
            @matrix[kk].each_key do |ii|
              producthash[jj][ii] = producthash[jj][ii] + ((m.matrix[jj][kk] * @matrix[kk][ii]).to_f).restrict($precision)
            end
          end
        end
        if (producthash[m.column_dimension][row_dimension].nil?)
          producthash[m.column_dimension][row_dimension] = 0
        end
        return SpMatrix.hash(producthash, true, m.column_dimension, row_dimension)
      else raise TypeError
      end
    end

    def **(exp)
      raise InvalidExponentError unless exp.integer?
      raise InvalidExponentError if exp < 0
      raise DimensionMismatchError.new(row_dimension,
                                       column_dimension,
                                       row_dimension,
                                       column_dimension) unless self.square?
      case exp
      when 0 then identity(row_dimension)
      when 1 then self
      when 2 then self * self
      else
        if exp.even?
          (self ** (exp/2)) ** 2
        else
          (self ** (exp.to_f/2).ceil) * self
        end
      end
    end
    
    def identity?
      return false if row_dimension != column_dimension
      id = true
      for ii in 1..@row_dimension
        (@matrix[ii][ii] == 1) ? (next) : (id = false; break)
      end
      return id
    end

    def diagonal?
      @matrix.keys.inject(true) { |res, ii| res and (@matrix[ii].keys == [ii]) }
    end

    def square?; (row_dimension == column_dimension) ? true : false; end

    def transpose
      tr = Hash.new { |h,k| h[k] = Hash.new(0) }
      @matrix.each_key do |jj|
        @matrix[jj].each_key do |ii|
          tr[ii][jj] = @matrix[jj][ii]
        end
      end
      SpMatrix.hash(tr, true, @row_dimension, @column_dimension)
    end

    def ==(other)
      return false unless SpMatrix === other
      @matrix == other.matrix
    end

    def [](i,j); @matrix[j][i]; end

    def set(i,j, val); @matrix[j][i] = val; end

    def getcolumn(j)
      unless ((j > 0) and (j <= column_dimension))
        raise ArgumentError.new("Invalid column specification: #{j}.") 
      end
      column = Array.new(row_dimension, 0)
      @matrix[j].each_pair { |ind, val| column[ind - 1] = val }
      column
    end

    def getrow(i)
      unless ((i > 0) and (i <= row_dimension))
        raise ArgumentError.new("Invalid row specification: #{i}.")
      end
      row = Array.new(column_dimension, 0)
      @matrix.each_pair do |ind, col|
        row[ind - 1] = ((col[i].nil?) ? 0 : col[i])
      end
      row
    end

    def getdiagonal
      raise ArgumentError.new("Not a square matrix. Unable to get diagonal.") unless square?
      diag = Array.new(row_dimension, 0)
      1.upto(row_dimension) { |ii| diag[ii-1] = @matrix[ii][ii] }
      diag
    end

    def trace
      raise ArgumentError.new("Not a square matrix. Unable to compute trace.") unless square?
      (1..row_dimension).inject(0) { |trace, ii| addcells(trace,@matrix[ii][ii]) }
    end

    def max
      elem = -(1.0/0)
      @matrix.each_pair do |col_ind, column|
        candidate = column.values.max
        (candidate > elem) ? (elem = candidate) : ()
      end
      elem
    end

    def min; (self * -1).max * -1; end

    def determinant
      unless square?
        self.printmatrix
        raise ArgumentError.new("Not a square matrix. Unable to compute determinant.")
      end
      if row_dimension == 1
        return @matrix[1][1]
      end
      det = 0
      (1..column_dimension).each do |cnum|
        ((1 + cnum).even?) ? (p = 1) : (p = -1)
        det += (@matrix[cnum][1]*p*(minor(cnum,1)).to_f).restrict($precision)
      end
      det
    end
    
    def minor(cnum, rnum)
      sub_ = Hash.new { |h,k| h[k] = Hash.new(0) }
      (1..column_dimension).each do |mcol|
        next if mcol == cnum
        (mcol > cnum) ? (t_col = mcol - 1) : (t_col = mcol)
        (1..row_dimension).each do |mrow|
          next if mrow == rnum
          (mrow > rnum) ? (t_row = mrow - 1) : (t_row = mrow)
          sub_[t_col][t_row] = @matrix[mcol][mrow]
        end
      end
      res = SpMatrix.hash(sub_, true)
      (sub_.empty?) ? (0) : (res.determinant)
    end

    def inverse
      inv_ = Hash.new { |h,k| h[k] = Hash.new(0) }
      det = self.determinant
      (1..row_dimension).each do |i|
        (1..column_dimension).each do |j|
          ((i + j).even?) ? (p = 1) : (p = -1)
          inv_[i][j] = (p*(minor(j,i)).to_f/det).restrict($precision)
        end
      end
      SpMatrix.hash(inv_, true)
    end

    def density
      size = row_dimension*column_dimension
      total = 0
      @matrix.each_pair { |cnum, col| total += col.values.size }
      total.to_f/size
    end

    def asVector
      arr = Array.new
      @matrix.each_pair { |cnum, col| arr << col.values }
      arr.flatten
    end

    def tomatrix
      t_columns = []
      for j in 1..column_dimension do
        t_columns << self.getcolumn(j)
      end
      Matrix.columns(t_columns)
    end

    def printmatrix
      for i in 1..row_dimension do
        print "["
        for j in 1...column_dimension do
          print self[i,j].to_s + ", "
        end
        print self[i,column_dimension].to_s + "]\n"
      end
    end

    def SpMatrix.printnormalmatrix(normalmatrix)
      normalmatrix.row_vectors.each do |rowvec|
        puts rowvec.to_a.to_s
      end
    end
    
    def print_
      s = "{ "
      @matrix.each_key do |c|
        s += "#{c} => { "
        @matrix[c].each_pair do |r, elem|
          s += "#{r} => #{elem}, "
        end
        s += "}, "
      end
      s + "}"
    end

    private :print_
    private :validkeys?
    private :addcells
  end
end
