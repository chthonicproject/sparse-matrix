############################################################
#                  sparseMatrixErrors.rb                   #
#                                                          #
#  Author: Ritwik Banerjee <rbanerjee@cs.stonybrook.edu>   #
#                                                          #
# This file provides the errors and exceptions for the     #
# sparse matrix class representation and operations.       #
############################################################

module SparseMatrix
  class InvalidKeyError < ArgumentError
    attr_reader :key

    def initialize(key); @key = key; end
    def to_s; "Invalid key specified: #{@key}"; end
    def inspect; to_s; end

  end

  class DimensionMismatchError < ArgumentError
    attr_reader :row1
    attr_reader :row2
    attr_reader :column1
    attr_reader :column2

    def initialize(row1, column1, row2, column2)
      @row1 = row1
      @row2 = row2
      @column1 = column1
      @column2 = column2
    end

    def to_s
      "Dimension mismatch: (#{@row1}*#{@column1}), (#{@row2}*#{@column2})"
    end

    def inspect; to_s; end
  end

  class InvalidExponentError < ArgumentError
    attr_reader :num

    def initialize(num); @num = num; end
    def to_s; "Exponent #{@num} is not an integer."; end
    def inspect; to_s; end

  end

end
