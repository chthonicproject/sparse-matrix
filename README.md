A quick sparse matrix implementation supporting the basic matrix
operations: addition, multiplication, exponentiation, matrix equality,
transpose, trace, determinant and the inverse of a matrix.

Other obvious methods include row and column retrieval, matrix entry retrieval and modification, and obtaining the minor of (i,j)th entry.

Since this is meant for sparse matrices, a method 'density' is also present, and computes the density of a sparse matrix instantiation.
